package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/pkg/helper"
)

type saleRepo struct {
	db *pgxpool.Pool
}

func NewSaleRepo(db *pgxpool.Pool) *saleRepo {
	return &saleRepo{
		db: db,
	}
}

func (r *saleRepo) Create(ctx context.Context, req *store_service.CreateSale) (resp *store_service.SalePrimaryKey, err error) {

	fmt.Printf("%+v",req)
	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO sale(
			id,
			sale_id,
			shift_id,
			branch_id,
			staff_id,
			market_id,
			status,
			created_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, NOW())
	`

	if req.Status==""{
		req.Status="in_process"
	}
	_, err = r.db.Exec(ctx, query,
		id,
		req.SaleId,
		req.ShiftId,
		req.BranchId,
		req.StaffId,
		req.MarketId,
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &store_service.SalePrimaryKey{Id: id}, nil
}

func (r *saleRepo) GetByID(ctx context.Context, req *store_service.SalePrimaryKey) (*store_service.Sale, error) {
	var (
		query      string
		id         sql.NullString
		sale_id    sql.NullString
		shift_id   sql.NullString
		branch_id  sql.NullString
		staff_id   sql.NullString
		market_id  sql.NullString
		status     sql.NullString
		created_at sql.NullString
	)

	query = `
		SELECT
			id,
			sale_id,
			shift_id,
			branch_id,
			staff_id,
			market_id,
			status,
			created_at
		FROM "sale"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&sale_id,
		&shift_id,
		&branch_id,
		&staff_id,
		&market_id,
		&status,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &store_service.Sale{
		Id:        id.String,
		SaleId:    sale_id.String,
		ShiftId:   shift_id.String,
		BranchId:  branch_id.String,
		StaffId:   staff_id.String,
		MarketId:  market_id.String,
		Status:    status.String,
		CreatedAt: created_at.String,
	}, nil
}

func (r *saleRepo) GetList(ctx context.Context, req *store_service.GetListSaleRequest) (*store_service.GetListSaleResponse, error) {

	var (
		resp   = &store_service.GetListSaleResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			sale_id,
			shift_id,
			branch_id,
			staff_id,
			market_id,
			status,
			created_at,
			updated_at
		FROM "sale"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SaleId != "" {
		where += ` AND sale_id = '` + req.SaleId + `'`
	}

	if req.ShiftId != "" {
		where += ` AND shift_id = '` + req.ShiftId + `'`
	}

	if req.BranchId != "" {
		where += ` AND branch_id = '` + req.BranchId + `'`
	}
	query += where + offset + limit


	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			id         sql.NullString
			sale_id    sql.NullString
			shift_id   sql.NullString
			branch_id  sql.NullString
			staff_id   sql.NullString
			market_id  sql.NullString
			status     sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&sale_id,
			&shift_id,
			&branch_id,
			&staff_id,
			&market_id,
			&status,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Sales = append(resp.Sales, &store_service.Sale{
			Id:        id.String,
			SaleId:    sale_id.String,
			ShiftId:   shift_id.String,
			BranchId:  branch_id.String,
			StaffId:   staff_id.String,
			MarketId:  market_id.String,
			Status:    status.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}

	return resp, nil
}

func (r *saleRepo) Update(ctx context.Context, req *store_service.UpdateSale) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"sale"
		SET
			sale_id =:sale_id,
			shift_id =:shift_id,
			branch_id =:branch_id,
			staff_id =:staff_id,
			market_id =:market_id,
			status =:status,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.GetId(),
		"sale_id":   req.GetSaleId(),
		"shift_id":  req.GetShiftId(),
		"branch_id": req.GetBranchId(),
		"staff_id":  req.GetStaffId(),
		"market_id": req.GetMarketId(),
		"status":    req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *saleRepo) Delete(ctx context.Context, req *store_service.SalePrimaryKey) (*store_service.SaleEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM sale WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &store_service.SaleEmpty{}, nil
}
