package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_store_service/config"
	"gitlab.com/market/mk_go_store_service/storage"
)

type Store struct {
	db                *pgxpool.Pool
	sale              *saleRepo
	saleProduct       *saleProductRepo
	payment           *paymentRepo
	shift             *shiftRepo
	shift_transaction *shift_transactionRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {

	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",

			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err

}
func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Sale() storage.SaleRepoI {

	if s.sale == nil {
		s.sale = NewSaleRepo(s.db)
	}

	return s.sale

}

func (s *Store) SaleProduct() storage.SaleProductRepoI {

	if s.saleProduct == nil {
		s.saleProduct = NewSaleProductRepo(s.db)
	}

	return s.saleProduct

}

func (s *Store) Payment() storage.PaymentRepoI {

	if s.payment == nil {
		s.payment = NewPaymentRepo(s.db)
	}

	return s.payment

}

func (s *Store) Shift() storage.ShiftRepoI {

	if s.shift == nil {
		s.shift = NewShiftRepo(s.db)
	}

	return s.shift

}

func (s *Store) ShiftTransaction() storage.ShiftTransactionRepoI {

	if s.shift_transaction == nil {
		s.shift_transaction = NewShiftTransactionRepo(s.db)
	}

	return s.shift_transaction

}
