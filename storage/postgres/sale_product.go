package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/pkg/helper"
)

type saleProductRepo struct {
	db *pgxpool.Pool
}

func NewSaleProductRepo(db *pgxpool.Pool) *saleProductRepo {
	return &saleProductRepo{
		db: db,
	}
}

func (r *saleProductRepo) Create(ctx context.Context, req *store_service.CreateSaleProduct) (resp *store_service.SaleProductPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO sale_product(
			id,
			sale_id,
			brand_id,
			category_id,
			name,
			barcode,
			price,
			amount,
			remain_amount,
			total_price,
			created_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7,$8, $9, $10, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.SaleId,
		req.BrandId,
		req.CategoryId,
		req.Name,
		req.Barcode,
		req.Price,
		req.Amount,
		req.RemainAmount,
		req.Price*float64(req.Amount),
	)

	if err != nil {
		return nil, err
	}

	return &store_service.SaleProductPrimaryKey{Id: id}, nil
}

func (r *saleProductRepo) GetByID(ctx context.Context, req *store_service.SaleProductPrimaryKey) (*store_service.SaleProduct, error) {
	var (
		query         string
		id            sql.NullString
		sale_id       sql.NullString
		brand_id      sql.NullString
		category_id   sql.NullString
		name          sql.NullString
		barcode       sql.NullString
		price         sql.NullFloat64
		amount        sql.NullInt64
		remain_amount sql.NullInt64
		total_price   sql.NullFloat64
		created_at    sql.NullString
	)

	query = `
		SELECT
			id,
			sale_id,
			brand_id,
			category_id,
			name,
			barcode,
			price,
			amount,
			remain_amount,
			total_price,
			created_at
		FROM "sale_product"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&sale_id,
		&brand_id,
		&category_id,
		&name,
		&barcode,
		&price,
		&amount,
		&remain_amount,
		&total_price,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &store_service.SaleProduct{
		Id:           id.String,
		SaleId:       sale_id.String,
		BrandId:      brand_id.String,
		CategoryId:   category_id.String,
		Name:         name.String,
		Barcode:      barcode.String,
		Price:        price.Float64,
		Amount:       amount.Int64,
		RemainAmount: remain_amount.Int64,
		TotalPrice:   total_price.Float64,
		CreatedAt:    created_at.String,
	}, nil
}

func (r *saleProductRepo) GetList(ctx context.Context, req *store_service.GetListSaleProductRequest) (*store_service.GetListSaleProductResponse, error) {

	fmt.Println()
	fmt.Printf("%+v", req)
	fmt.Println()
	var (
		resp   = &store_service.GetListSaleProductResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			sale_id,
			brand_id,
			category_id,
			name,
			barcode,
			price,
			amount,
			remain_amount,
			total_price,
			created_at,
			updated_at
		FROM "sale_product"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SaleId != "" {
		where += ` AND sale_id = '` + req.SaleId + `'`
	}
	
	if req.BrandId != "" {
		where += ` AND brand_id = '` + req.BrandId + `'`
	}

	if req.Barcode != "" {
		where += ` AND barcode = '` + req.Barcode + `'`
	}

	if req.CategoryId != "" {
		where += ` AND category_id = '` + req.CategoryId + `'`
	}

	query += where + offset + limit

	fmt.Println(query)
	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	
	defer rows.Close()

	for rows.Next() {
		var (
			id            sql.NullString
			sale_id       sql.NullString
			brand_id      sql.NullString
			category_id   sql.NullString
			name          sql.NullString
			barcode       sql.NullString
			price         sql.NullFloat64
			amount        sql.NullInt64
			remain_amount sql.NullInt64
			total_price   sql.NullFloat64
			created_at    sql.NullString
			updated_at    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&sale_id,
			&brand_id,
			&category_id,
			&name,
			&barcode,
			&price,
			&amount,
			&remain_amount,
			&total_price,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.SaleProducts = append(resp.SaleProducts, &store_service.SaleProduct{
			Id:           id.String,
			SaleId:       sale_id.String,
			BrandId:      brand_id.String,
			CategoryId:   category_id.String,
			Name:         name.String,
			Barcode:      barcode.String,
			Price:        price.Float64,
			Amount:       amount.Int64,
			RemainAmount: remain_amount.Int64,
			TotalPrice:   total_price.Float64,
			CreatedAt:    created_at.String,
			UpdatedAt:    updated_at.String,
		})
	}

	return resp, nil
}

func (r *saleProductRepo) Update(ctx context.Context, req *store_service.UpdateSaleProduct) (int64, error) {

	fmt.Printf("%+v", req)
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"sale_product"
		SET
			sale_id = :sale_id ,
			brand_id = :brand_id ,
			category_id = :category_id ,
			name = :name ,
			barcode = :barcode ,
			price = :price ,
			amount = :amount ,
			remain_amount = :remain_amount ,
			total_price = :total_price ,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":            req.GetId(),
		"sale_id":       req.GetSaleId(),
		"brand_id":      req.GetBrandId(),
		"category_id":   req.GetCategoryId(),
		"name":          req.GetName(),
		"barcode":       req.GetBarcode(),
		"price":         req.GetPrice(),
		"amount":        req.GetAmount(),
		"remain_amount": req.GetRemainAmount(),
		"total_price":   req.GetPrice() * float64(req.GetAmount()),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *saleProductRepo) Delete(ctx context.Context, req *store_service.SaleProductPrimaryKey) (*store_service.SaleProductEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM sale_product WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &store_service.SaleProductEmpty{}, nil
}
