package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/pkg/helper"
)

type shiftRepo struct {
	db *pgxpool.Pool
}

func NewShiftRepo(db *pgxpool.Pool) *shiftRepo {
	return &shiftRepo{
		db: db,
	}
}

func (r *shiftRepo) Create(ctx context.Context, req *store_service.CreateShift) (resp *store_service.ShiftPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO shift(
			id,
			shift_id,
			branch_id,
			staff_id,
			market_id,
			created_at)
		VALUES ($1, $2, $3, $4, $5, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.ShiftId,
		req.BranchId,
		req.StaffId,
		req.MarketId,
	)

	if err != nil {
		return nil, err
	}

	return &store_service.ShiftPrimaryKey{Id: id}, nil
}

func (r *shiftRepo) GetByID(ctx context.Context, req *store_service.ShiftPrimaryKey) (*store_service.Shift, error) {
	var (
		query      string
		id         sql.NullString
		shift_id   sql.NullString
		branch_id  sql.NullString
		staff_id   sql.NullString
		market_id  sql.NullString
		status     sql.NullString
		created_at sql.NullString
	)

	query = `
		SELECT
			id,
			shift_id,
			branch_id,
			staff_id,
			market_id,
			status,
			created_at
		FROM "shift"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&shift_id,
		&branch_id,
		&staff_id,
		&market_id,
		&status,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &store_service.Shift{
		Id:        id.String,
		ShiftId:   shift_id.String,
		BranchId:  branch_id.String,
		StaffId:   staff_id.String,
		MarketId:  market_id.String,
		Status:    status.String,
		CreatedAt: created_at.String,
	}, nil
}

func (r *shiftRepo) GetList(ctx context.Context, req *store_service.GetListShiftRequest) (*store_service.GetListShiftResponse, error) {

	var (
		resp   = &store_service.GetListShiftResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			shift_id,
			branch_id,
			staff_id,
			market_id,
			status,
			created_at,
			updated_at
		FROM "shift"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Status != "" {
		where += ` AND status ILIKE '%open%' `
	}
	if req.StaffId != "" {
		where += ` AND staff_id = '` + req.StaffId + `'`
	}

	if req.BranchId != "" {
		where += ` AND branch_id = '` + req.BranchId + `'`
	}

	if req.ShiftId != "" {
		where += ` AND shift_id = '` + req.ShiftId + `'`
	}


	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	
	defer rows.Close()
	
	for rows.Next() {
		var (
			id         sql.NullString
			shift_id   sql.NullString
			branch_id  sql.NullString
			staff_id   sql.NullString
			market_id  sql.NullString
			status     sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&shift_id,
			&branch_id,
			&staff_id,
			&market_id,
			&status,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Shifts = append(resp.Shifts, &store_service.Shift{
			Id:        id.String,
			ShiftId:   shift_id.String,
			BranchId:  branch_id.String,
			StaffId:   staff_id.String,
			MarketId:  market_id.String,
			Status:    status.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}

	return resp, nil
}

func (r *shiftRepo) Update(ctx context.Context, req *store_service.UpdateShift) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"shift"
		SET
			shift_id =:shift_id,
			branch_id =:branch_id,
			staff_id =:staff_id,
			market_id =:market_id,
			status =:status,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.GetId(),
		"shift_id":  req.GetShiftId(),
		"branch_id": req.GetBranchId(),
		"staff_id":  req.GetStaffId(),
		"market_id": req.GetMarketId(),
		"status":    req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *shiftRepo) Delete(ctx context.Context, req *store_service.ShiftPrimaryKey) (*store_service.ShiftEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM shift WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &store_service.ShiftEmpty{}, nil
}



