package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/pkg/helper"
)

type shift_transactionRepo struct {
	db *pgxpool.Pool
}

func NewShiftTransactionRepo(db *pgxpool.Pool) *shift_transactionRepo {
	return &shift_transactionRepo{
		db: db,
	}
}

func (r *shift_transactionRepo) Create(ctx context.Context, req *store_service.CreateShiftTransaction) (resp *store_service.ShiftTransactionPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO shift_transaction(
			id,
			shift_id,
			cash,
			uzcard,
			humo,
			payme,
			click,
			apelsin,
			visa,
			currency,
			exchange_rate,
			total_price,
			created_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.ShiftId,
		req.Cash,
		req.Uzcard,
		req.Humo,
		req.Payme,
		req.Click,
		req.Apelsin,
		req.Visa,
		req.Currency,
		req.ExchangeRate,
		(req.Cash+req.Uzcard+req.Humo+req.Payme+req.Click+req.Apelsin+req.Visa)*req.ExchangeRate,
	)

	if err != nil {
		return nil, err
	}

	return &store_service.ShiftTransactionPrimaryKey{Id: id}, nil
}

func (r *shift_transactionRepo) GetByID(ctx context.Context, req *store_service.ShiftTransactionPrimaryKey) (*store_service.ShiftTransaction, error) {
	var (
		query         string
		id            sql.NullString
		shift_id      sql.NullString
		cash          sql.NullFloat64
		uzcard        sql.NullFloat64
		humo          sql.NullFloat64
		payme         sql.NullFloat64
		click         sql.NullFloat64
		apelsin       sql.NullFloat64
		visa          sql.NullFloat64
		currency      sql.NullString
		exchange_rate sql.NullFloat64
		total_price   sql.NullFloat64
		created_at    sql.NullString
	)

	query = `
		SELECT
			id,
			shift_id,
			cash,
			uzcard,
			humo,
			payme,
			click,
			apelsin,
			visa,
			currency,
			exchange_rate,
			total_price,
			created_at
		FROM "shift_transaction"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&shift_id,
		&cash,
		&uzcard,
		&humo,
		&payme,
		&click,
		&apelsin,
		&visa,
		&currency,
		exchange_rate,
		&total_price,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &store_service.ShiftTransaction{
		Id:           id.String,
		ShiftId:      shift_id.String,
		Cash:         cash.Float64,
		Uzcard:       uzcard.Float64,
		Humo:         humo.Float64,
		Payme:        payme.Float64,
		Click:        click.Float64,
		Apelsin:      apelsin.Float64,
		Visa:         visa.Float64,
		Currency:     currency.String,
		ExchangeRate: exchange_rate.Float64,
		TotalPrice:   total_price.Float64,
		CreatedAt:    created_at.String,
	}, nil
}

func (r *shift_transactionRepo) GetList(ctx context.Context, req *store_service.GetListShiftTransactionRequest) (*store_service.GetListShiftTransactionResponse, error) {

	var (
		resp   = &store_service.GetListShiftTransactionResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			shift_id,
			cash,
			uzcard,
			humo,
			payme,
			click,
			apelsin,
			visa,
			currency,
			exchange_rate,
			total_price,
			created_at,
			updated_at
		FROM "shift_transaction"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}


	if req.ShiftId != "" {
		where += ` AND shift_id = '` + req.ShiftId + `'`
	}
	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			id            sql.NullString
			shift_id      sql.NullString
			cash          sql.NullFloat64
			uzcard        sql.NullFloat64
			humo          sql.NullFloat64
			payme         sql.NullFloat64
			click         sql.NullFloat64
			apelsin       sql.NullFloat64
			visa          sql.NullFloat64
			currency      sql.NullString
			exchange_rate sql.NullFloat64
			total_price   sql.NullFloat64
			created_at    sql.NullString
			updated_at    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&shift_id,
			&cash,
			&uzcard,
			&humo,
			&payme,
			&click,
			&apelsin,
			&visa,
			&currency,
			exchange_rate,
			&total_price,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.ShiftTransactions = append(resp.ShiftTransactions, &store_service.ShiftTransaction{
			Id:           id.String,
			ShiftId:      shift_id.String,
			Cash:         cash.Float64,
			Uzcard:       uzcard.Float64,
			Humo:         humo.Float64,
			Payme:        payme.Float64,
			Click:        click.Float64,
			Apelsin:      apelsin.Float64,
			Visa:         visa.Float64,
			Currency:     currency.String,
			ExchangeRate: exchange_rate.Float64,
			TotalPrice:   total_price.Float64,
			CreatedAt:    created_at.String,
			UpdatedAt:    updated_at.String,
		})
	}

	return resp, nil
}

func (r *shift_transactionRepo) Update(ctx context.Context, req *store_service.UpdateShiftTransaction) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"shift_transaction"
		SET
			shift_id = :shift_id,
			cash = :cash,
			uzcard = :uzcard,
			humo = :humo,
			payme = :payme,
			click = :click,
			apelsin = :apelsin,
			visa = :visa,
			currency = :currency,
			exchange_rate = :exchange_rate,
			total_price = :total_price,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":            req.GetId(),
		"shift_id":      req.GetShiftId(),
		"uzcard":        req.GetUzcard(),
		"humo":          req.GetHumo(),
		"cash":          req.GetCash(),
		"payme":         req.GetPayme(),
		"click":         req.GetClick(),
		"apelsin":       req.GetApelsin(),
		"visa":          req.GetVisa(),
		"currency":      req.GetCurrency(),
		"exchange_rate": req.GetExchangeRate(),
		"total_price":   (req.GetUzcard() + req.GetHumo() + req.GetCash() + req.GetPayme() + req.GetClick() + req.GetApelsin() + req.GetVisa() ) * req.GetExchangeRate(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *shift_transactionRepo) Delete(ctx context.Context, req *store_service.ShiftTransactionPrimaryKey) (*store_service.ShiftTransactionEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM shift_transaction WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &store_service.ShiftTransactionEmpty{}, nil
}
