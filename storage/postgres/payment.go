package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/pkg/helper"
)

type paymentRepo struct {
	db *pgxpool.Pool
}

func NewPaymentRepo(db *pgxpool.Pool) *paymentRepo {
	return &paymentRepo{
		db: db,
	}
}

func (r *paymentRepo) Create(ctx context.Context, req *store_service.CreatePayment) (resp *store_service.PaymentPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO payment(
			id,
			sale_id,
			cash,
			uzcard,
			humo,
			payme,
			click,
			apelsin,
			visa,
			currency,
			exchange_rate,
			total_price,
			created_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7,$8, $9, $10, $11, $12, NOW())
	`

	
	_, err = r.db.Exec(ctx, query,
		id,
		req.SaleId,
		req.Cash,
		req.Uzcard,
		req.Humo,
		req.Payme,
		req.Click,
		req.Apelsin,
		req.Visa,
		req.Currency,
		req.ExchangeRate,
		(req.Cash+req.Uzcard+req.Humo+req.Payme+req.Click+req.Apelsin+req.Visa)*req.ExchangeRate,
	)

	if err != nil {
		return nil, err
	}

	return &store_service.PaymentPrimaryKey{Id: id}, nil
}

func (r *paymentRepo) GetByID(ctx context.Context, req *store_service.PaymentPrimaryKey) (*store_service.Payment, error) {
	var (
		query         string
		id            sql.NullString
		sale_id       sql.NullString
		cash          sql.NullFloat64
		uzcard        sql.NullFloat64
		humo          sql.NullFloat64
		payme         sql.NullFloat64
		click         sql.NullFloat64
		apelsin       sql.NullFloat64
		visa          sql.NullFloat64
		currency      sql.NullString
		exchange_rate sql.NullFloat64
		total_price   sql.NullFloat64
		created_at    sql.NullString
	)

	query = `
		SELECT
			id,
			sale_id,
			cash,
			uzcard,
			humo,
			payme,
			click,
			apelsin,
			visa,
			currency,
			exchange_rate,
			total_price,
			created_at
		FROM "payment"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&sale_id,
		&cash,
		&uzcard,
		&humo,
		&payme,
		&click,
		&apelsin,
		&visa,
		&currency,
		exchange_rate,
		&total_price,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &store_service.Payment{
		Id:           id.String,
		SaleId:       sale_id.String,
		Cash:         cash.Float64,
		Uzcard:       uzcard.Float64,
		Humo:         humo.Float64,
		Payme:        payme.Float64,
		Click:        click.Float64,
		Apelsin:      apelsin.Float64,
		Visa:         visa.Float64,
		Currency:     currency.String,
		ExchangeRate: exchange_rate.Float64,
		TotalPrice:   total_price.Float64,
		CreatedAt:    created_at.String,
	}, nil
}

func (r *paymentRepo) GetList(ctx context.Context, req *store_service.GetListPaymentRequest) (*store_service.GetListPaymentResponse, error) {

	var (
		resp   = &store_service.GetListPaymentResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			sale_id,
			cash,
			uzcard,
			humo,
			payme,
			click,
			apelsin,
			visa,
			currency,
			exchange_rate,
			total_price,
			created_at,
			updated_at
		FROM "payment"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND sale_id = '` + req.Search + `'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			id            sql.NullString
			sale_id       sql.NullString
			cash          sql.NullFloat64
			uzcard        sql.NullFloat64
			humo          sql.NullFloat64
			payme         sql.NullFloat64
			click         sql.NullFloat64
			apelsin       sql.NullFloat64
			visa          sql.NullFloat64
			currency      sql.NullString
			exchange_rate sql.NullFloat64
			total_price   sql.NullFloat64
			created_at    sql.NullString
			updated_at    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&sale_id,
			&cash,
			&uzcard,
			&humo,
			&payme,
			&click,
			&apelsin,
			&visa,
			&currency,
			exchange_rate,
			&total_price,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Payments = append(resp.Payments, &store_service.Payment{
			Id:           id.String,
			SaleId:       sale_id.String,
			Cash:         cash.Float64,
			Uzcard:       uzcard.Float64,
			Humo:         humo.Float64,
			Payme:        payme.Float64,
			Click:        click.Float64,
			Apelsin:      apelsin.Float64,
			Visa:         visa.Float64,
			Currency:     currency.String,
			ExchangeRate: exchange_rate.Float64,
			TotalPrice:   total_price.Float64,
			CreatedAt:    created_at.String,
			UpdatedAt:    updated_at.String,
		})
	}


	return resp, nil
}

func (r *paymentRepo) Update(ctx context.Context, req *store_service.UpdatePayment) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"payment"
		SET
			sale_id = :sale_id,
			cash = :cash,
			uzcard = :uzcard,
			humo = :humo,
			payme = :payme,
			click = :click,
			apelsin = :apelsin,
			visa = :visa,
			currency = :currency,
			exchange_rate = :exchange_rate,
			total_price = :total_price,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":         req.GetId(),
		"sale_id": req.GetSaleId(),
		"cash": req.GetCash(),
		"uzcard": req.GetUzcard(),
		"humo": req.GetHumo(),
		"payme": req.GetPayme(),
		"click": req.GetClick(),
		"apelsin": req.GetApelsin(),
		"visa": req.GetVisa(),
		"currency": req.GetCurrency(),
		"exchange_rate": req.GetExchangeRate(),
		"total_price": (req.GetUzcard() + req.GetHumo() + req.GetCash() + req.GetPayme() + req.GetClick() + req.GetApelsin() + req.GetVisa() ) * req.GetExchangeRate(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *paymentRepo) Delete(ctx context.Context, req *store_service.PaymentPrimaryKey) (*store_service.PaymentEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM payment WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &store_service.PaymentEmpty{}, nil
}
