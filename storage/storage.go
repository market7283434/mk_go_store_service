package storage

import (
	"context"

	"gitlab.com/market/mk_go_store_service/genproto/store_service"
)

type StorageI interface {
	CloseDB()
	Sale() SaleRepoI
	SaleProduct() SaleProductRepoI
	Payment() PaymentRepoI
	Shift() ShiftRepoI
	ShiftTransaction() ShiftTransactionRepoI
}

type SaleRepoI interface {
	Create(ctx context.Context, req *store_service.CreateSale) (resp *store_service.SalePrimaryKey, err error)
	GetByID(ctx context.Context, req *store_service.SalePrimaryKey) (resp *store_service.Sale, err error)
	GetList(ctx context.Context, req *store_service.GetListSaleRequest) (resp *store_service.GetListSaleResponse, err error)
	Update(ctx context.Context, req *store_service.UpdateSale) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *store_service.SalePrimaryKey) (*store_service.SaleEmpty, error)
}

type PaymentRepoI interface {
	Create(ctx context.Context, req *store_service.CreatePayment) (resp *store_service.PaymentPrimaryKey, err error)
	GetByID(ctx context.Context, req *store_service.PaymentPrimaryKey) (resp *store_service.Payment, err error)
	GetList(ctx context.Context, req *store_service.GetListPaymentRequest) (resp *store_service.GetListPaymentResponse, err error)
	Update(ctx context.Context, req *store_service.UpdatePayment) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *store_service.PaymentPrimaryKey) (*store_service.PaymentEmpty, error)
}

type ShiftRepoI interface {
	Create(ctx context.Context, req *store_service.CreateShift) (resp *store_service.ShiftPrimaryKey, err error)
	GetByID(ctx context.Context, req *store_service.ShiftPrimaryKey) (resp *store_service.Shift, err error)
	GetList(ctx context.Context, req *store_service.GetListShiftRequest) (resp *store_service.GetListShiftResponse, err error)
	Update(ctx context.Context, req *store_service.UpdateShift) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *store_service.ShiftPrimaryKey) (*store_service.ShiftEmpty, error)
}

type SaleProductRepoI interface {
	Create(ctx context.Context, req *store_service.CreateSaleProduct) (resp *store_service.SaleProductPrimaryKey, err error)
	GetByID(ctx context.Context, req *store_service.SaleProductPrimaryKey) (resp *store_service.SaleProduct, err error)
	GetList(ctx context.Context, req *store_service.GetListSaleProductRequest) (resp *store_service.GetListSaleProductResponse, err error)
	Update(ctx context.Context, req *store_service.UpdateSaleProduct) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *store_service.SaleProductPrimaryKey) (*store_service.SaleProductEmpty, error)
}

type ShiftTransactionRepoI interface {
	Create(ctx context.Context, req *store_service.CreateShiftTransaction) (resp *store_service.ShiftTransactionPrimaryKey, err error)
	GetByID(ctx context.Context, req *store_service.ShiftTransactionPrimaryKey) (resp *store_service.ShiftTransaction, err error)
	GetList(ctx context.Context, req *store_service.GetListShiftTransactionRequest) (resp *store_service.GetListShiftTransactionResponse, err error)
	Update(ctx context.Context, req *store_service.UpdateShiftTransaction) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *store_service.ShiftTransactionPrimaryKey) (*store_service.ShiftTransactionEmpty, error)
}