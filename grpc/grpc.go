package grpc

import (
	"gitlab.com/market/mk_go_store_service/config"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/grpc/client"
	"gitlab.com/market/mk_go_store_service/grpc/service"
	"gitlab.com/market/mk_go_store_service/pkg/logger"
	"gitlab.com/market/mk_go_store_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()
	store_service.RegisterSaleServiceServer(grpcServer, service.NewSaleService(cfg, log, strg, srvc))
	store_service.RegisterSaleProductServiceServer(grpcServer, service.NewSaleProductService(cfg, log, strg, srvc))
	store_service.RegisterPaymentServiceServer(grpcServer, service.NewPaymentService(cfg, log, strg, srvc))
	store_service.RegisterShiftServiceServer(grpcServer, service.NewShiftService(cfg, log, strg, srvc))
	store_service.RegisterShiftTransactionServiceServer(grpcServer, service.NewShiftTransactionService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
