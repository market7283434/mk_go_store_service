package service

import (
	"context"

	"gitlab.com/market/mk_go_store_service/config"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/grpc/client"
	"gitlab.com/market/mk_go_store_service/pkg/logger"
	"gitlab.com/market/mk_go_store_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type PaymentService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*store_service.UnimplementedPaymentServiceServer
}

func NewPaymentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *PaymentService {
	return &PaymentService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *PaymentService) Create(ctx context.Context, req *store_service.CreatePayment) (resp *store_service.Payment, err error) {
	i.log.Info("---------CreatePayment---------", logger.Any("req", req))

	id, err := i.strg.Payment().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreatePayment->Payment->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Payment().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetPaymentById --> Create Response->Payment", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *PaymentService) GetByID(ctx context.Context, req *store_service.PaymentPrimaryKey) (resp *store_service.Payment, err error) {
	i.log.Info("---------GetPaymentById---------", logger.Any("req", req))

	resp, err = i.strg.Payment().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetPaymentById->Payment->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *PaymentService) GetList(ctx context.Context, req *store_service.GetListPaymentRequest) (resp *store_service.GetListPaymentResponse, err error) {

	i.log.Info("---GetPayments------>", logger.Any("req", req))

	resp, err = i.strg.Payment().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetPayments->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *PaymentService) Update(ctx context.Context, req *store_service.UpdatePayment) (resp *store_service.Payment, err error) {

	i.log.Info("---UpdatePayment------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Payment().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdatePayment--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Payment().GetByID(ctx, &store_service.PaymentPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetPayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *PaymentService) Delete(ctx context.Context, req *store_service.PaymentPrimaryKey) (resp *store_service.PaymentEmpty, err error) {

	i.log.Info("---DeletePayment------>", logger.Any("req", req))

	resp, err = i.strg.Payment().Delete(ctx, &store_service.PaymentPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeletePayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
