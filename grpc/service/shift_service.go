package service

import (
	"context"

	"gitlab.com/market/mk_go_store_service/config"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/grpc/client"
	"gitlab.com/market/mk_go_store_service/pkg/logger"
	"gitlab.com/market/mk_go_store_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ShiftService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*store_service.UnimplementedShiftServiceServer
}

func NewShiftService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ShiftService {
	return &ShiftService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *ShiftService) Create(ctx context.Context, req *store_service.CreateShift) (resp *store_service.Shift, err error) {
	i.log.Info("---------CreateShift---------", logger.Any("req", req))

	id, err := i.strg.Shift().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateShift->Shift->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	

	resp, err = i.strg.Shift().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetShiftById --> Create Response->Shift", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ShiftService) GetByID(ctx context.Context, req *store_service.ShiftPrimaryKey) (resp *store_service.Shift, err error) {
	i.log.Info("---------GetShiftById---------", logger.Any("req", req))

	resp, err = i.strg.Shift().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShiftById->Shift->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ShiftService) GetList(ctx context.Context, req *store_service.GetListShiftRequest) (resp *store_service.GetListShiftResponse, err error) {

	i.log.Info("---GetShifts------>", logger.Any("req", req))

	resp, err = i.strg.Shift().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShifts->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShiftService) Update(ctx context.Context, req *store_service.UpdateShift) (resp *store_service.Shift, err error) {

	i.log.Info("---UpdateShift------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Shift().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateShift--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Shift().GetByID(ctx, &store_service.ShiftPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetShift->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ShiftService) Delete(ctx context.Context, req *store_service.ShiftPrimaryKey) (resp *store_service.ShiftEmpty, err error) {

	i.log.Info("---DeleteShift------>", logger.Any("req", req))

	resp, err = i.strg.Shift().Delete(ctx, &store_service.ShiftPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteShift->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
