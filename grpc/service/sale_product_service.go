package service

import (
	"context"

	"gitlab.com/market/mk_go_store_service/config"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/grpc/client"
	"gitlab.com/market/mk_go_store_service/pkg/logger"
	"gitlab.com/market/mk_go_store_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SaleProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*store_service.UnimplementedSaleProductServiceServer
}

func NewSaleProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SaleProductService {
	return &SaleProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *SaleProductService) Create(ctx context.Context, req *store_service.CreateSaleProduct) (resp *store_service.SaleProduct, err error) {
	i.log.Info("---------CreateSaleProduct---------", logger.Any("req", req))

	id, err := i.strg.SaleProduct().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSaleProduct->SaleProduct->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.SaleProduct().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetSaleProductById --> Create Response->SaleProduct", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *SaleProductService) GetByID(ctx context.Context, req *store_service.SaleProductPrimaryKey) (resp *store_service.SaleProduct, err error) {
	i.log.Info("---------GetSaleProductById---------", logger.Any("req", req))

	resp, err = i.strg.SaleProduct().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSaleProductById->SaleProduct->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *SaleProductService) GetList(ctx context.Context, req *store_service.GetListSaleProductRequest) (resp *store_service.GetListSaleProductResponse, err error) {

	i.log.Info("---GetSaleProducts------>", logger.Any("req", req))

	resp, err = i.strg.SaleProduct().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSaleProducts->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleProductService) Update(ctx context.Context, req *store_service.UpdateSaleProduct) (resp *store_service.SaleProduct, err error) {

	i.log.Info("---UpdateSaleProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.SaleProduct().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSaleProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.SaleProduct().GetByID(ctx, &store_service.SaleProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSaleProduct->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SaleProductService) Delete(ctx context.Context, req *store_service.SaleProductPrimaryKey) (resp *store_service.SaleProductEmpty, err error) {

	i.log.Info("---DeleteSaleProduct------>", logger.Any("req", req))

	resp, err = i.strg.SaleProduct().Delete(ctx, &store_service.SaleProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteSaleProduct->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
