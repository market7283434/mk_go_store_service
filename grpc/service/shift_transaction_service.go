package service

import (
	"context"

	"gitlab.com/market/mk_go_store_service/config"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/grpc/client"
	"gitlab.com/market/mk_go_store_service/pkg/logger"
	"gitlab.com/market/mk_go_store_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ShiftTransactionService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*store_service.UnimplementedShiftTransactionServiceServer
}

func NewShiftTransactionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ShiftTransactionService {
	return &ShiftTransactionService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *ShiftTransactionService) Create(ctx context.Context, req *store_service.CreateShiftTransaction) (resp *store_service.ShiftTransaction, err error) {
	i.log.Info("---------CreateShiftTransaction---------", logger.Any("req", req))

	id, err := i.strg.ShiftTransaction().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateShiftTransaction->ShiftTransaction->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.ShiftTransaction().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetShiftTransactionById --> Create Response->ShiftTransaction", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ShiftTransactionService) GetByID(ctx context.Context, req *store_service.ShiftTransactionPrimaryKey) (resp *store_service.ShiftTransaction, err error) {
	i.log.Info("---------GetShiftTransactionById---------", logger.Any("req", req))

	resp, err = i.strg.ShiftTransaction().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShiftTransactionById->ShiftTransaction->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ShiftTransactionService) GetList(ctx context.Context, req *store_service.GetListShiftTransactionRequest) (resp *store_service.GetListShiftTransactionResponse, err error) {

	i.log.Info("---GetShiftTransactions------>", logger.Any("req", req))

	resp, err = i.strg.ShiftTransaction().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShiftTransactions->ShiftTransaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShiftTransactionService) Update(ctx context.Context, req *store_service.UpdateShiftTransaction) (resp *store_service.ShiftTransaction, err error) {

	i.log.Info("---UpdateShiftTransaction------>", logger.Any("req", req))

	rowsAffected, err := i.strg.ShiftTransaction().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateShiftTransaction--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ShiftTransaction().GetByID(ctx, &store_service.ShiftTransactionPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetShiftTransaction->ShiftTransaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ShiftTransactionService) Delete(ctx context.Context, req *store_service.ShiftTransactionPrimaryKey) (resp *store_service.ShiftTransactionEmpty, err error) {

	i.log.Info("---DeleteShiftTransaction------>", logger.Any("req", req))

	resp, err = i.strg.ShiftTransaction().Delete(ctx, &store_service.ShiftTransactionPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteShiftTransaction->ShiftTransaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
