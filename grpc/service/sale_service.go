package service

import (
	"context"

	"gitlab.com/market/mk_go_store_service/config"
	"gitlab.com/market/mk_go_store_service/genproto/store_service"
	"gitlab.com/market/mk_go_store_service/grpc/client"
	"gitlab.com/market/mk_go_store_service/pkg/logger"
	"gitlab.com/market/mk_go_store_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SaleService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*store_service.UnimplementedSaleServiceServer
}

func NewSaleService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SaleService {
	return &SaleService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *SaleService) Create(ctx context.Context, req *store_service.CreateSale) (resp *store_service.Sale, err error) {
	i.log.Info("---------CreateSale---------", logger.Any("req", req))

	id, err := i.strg.Sale().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSale->Sale->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Sale().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetSaleById --> Create Response->Sale", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *SaleService) GetByID(ctx context.Context, req *store_service.SalePrimaryKey) (resp *store_service.Sale, err error) {
	i.log.Info("---------GetSaleById---------", logger.Any("req", req))

	resp, err = i.strg.Sale().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSaleById->Sale->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *SaleService) GetList(ctx context.Context, req *store_service.GetListSaleRequest) (resp *store_service.GetListSaleResponse, err error) {

	i.log.Info("---GetSales------>", logger.Any("req", req))

	resp, err = i.strg.Sale().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSales->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleService) Update(ctx context.Context, req *store_service.UpdateSale) (resp *store_service.Sale, err error) {

	i.log.Info("---UpdateSale------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Sale().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSale--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Sale().GetByID(ctx, &store_service.SalePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSale->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SaleService) Delete(ctx context.Context, req *store_service.SalePrimaryKey) (resp *store_service.SaleEmpty, err error) {

	i.log.Info("---DeleteSale------>", logger.Any("req", req))

	resp, err = i.strg.Sale().Delete(ctx, &store_service.SalePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteSale->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
