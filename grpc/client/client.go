package client

import (
	"gitlab.com/market/mk_go_store_service/config"
	"gitlab.com/market/mk_go_store_service/genproto/load_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	ProductService() load_service.ProductServiceClient
	RemainderService() load_service.RemainderServiceClient
}

type grpcClients struct {
	productService load_service.ProductServiceClient
	remainderService load_service.RemainderServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	connLOADService, err := grpc.Dial(
		cfg.LOADServiceHost+cfg.LOADGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	return &grpcClients{
		productService: load_service.NewProductServiceClient(connLOADService),
		remainderService: load_service.NewRemainderServiceClient(connLOADService),
	}, nil
}


func (g *grpcClients) ProductService() load_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) RemainderService() load_service.RemainderServiceClient {
	return g.remainderService
}