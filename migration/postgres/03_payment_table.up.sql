
CREATE TABLE "payment"(
    "id" UUID PRIMARY KEY,
    "sale_id" VARCHAR NOT NULL,
    "cash"  NUMERIC NOT NULL,
    "uzcard"  NUMERIC NOT NULL,
    "humo"  NUMERIC NOT NULL,
    "payme"  NUMERIC NOT NULL,
    "click"  NUMERIC NOT NULL,
    "apelsin"  NUMERIC NOT NULL,
    "visa"  NUMERIC NOT NULL,
    "currency"  NUMERIC NOT NULL,
    "exchange_rate"  NUMERIC NOT NULL,
    "total_price"  NUMERIC NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);



