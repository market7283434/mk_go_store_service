CREATE TABLE "shift_transaction"(
    "id" UUID PRIMARY KEY,
    "shift_id" UUID REFERENCES "shift"("id"),
    "cash"  NUMERIC ,
    "uzcard"  NUMERIC ,
    "humo"  NUMERIC ,
    "payme"  NUMERIC ,
    "click"  NUMERIC ,
    "apelsin"  NUMERIC ,
    "visa"  NUMERIC ,
    "currency"  VARCHAR ,
    "exchange_rate"  NUMERIC ,
    "total_price"  NUMERIC ,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);
