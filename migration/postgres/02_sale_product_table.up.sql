
CREATE TABLE "sale_product"(
    "id" UUID PRIMARY KEY,
    "sale_id" UUID REFERENCES "sale"("id"),
    "brand_id" VARCHAR NOT NULL,
    "category_id" UUID NOT NULL,
    "name" VARCHAR NOT NULL,
    "barcode" VARCHAR NOT NULL,
    "amount" INT NOT NULL,
    "remain_amount" INT NOT NULL,
    "price" NUMERIC NOT NULL,
    "total_price" NUMERIC NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);









