CREATE TABLE "shift"(
    "id" UUID PRIMARY KEY,
    "shift_id" VARCHAR NOT NULL,
    "branch_id" UUID NOT NULL,
    "staff_id" UUID NOT NULL,
    "status" VARCHAR DEFAULT 'open',
    "market_id" UUID NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);
