
CREATE TABLE "sale"(
    "id" UUID PRIMARY KEY,
    "sale_id" VARCHAR NOT NULL,
    "shift_id" VARCHAR NOT NULL,
    "branch_id" UUID NOT NULL,
    "staff_id" UUID NOT NULL,
    "market_id" UUID NOT NULL,
    "status" VARCHAR DEFAULT 'in_process',
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

